#include <Adafruit_Si7021.h>
#include <PubSubClient.h>
#include <WiFi.h>

const char* ssid = "stage3";
const char* pass = "stage123!";
const char* mqtt_server = "192.168.223.115";
const int   mqtt_port = 1883;

WiFiClient client;
PubSubClient mqttClient(client);
Adafruit_Si7021 sensor = Adafruit_Si7021();
char msg[10];


void setup() {
  Serial.begin(9600);
  connect_to_wifi();
  mqttClient.setServer(mqtt_server, mqtt_port);
  sensor.begin();

}

void loop() {
  int hum = sensor.readHumidity();
  float tem = sensor.readTemperature();
  Serial.println(tem);
  publush_message(tem, hum);
  delay(1000);
}

void publush_message(int tem, int hum){
  if (mqttClient.connect("my_measuring_device")){
    sprintf(msg, "%d,%d", tem, hum);
    mqttClient.publish("/mydevice/telemetry", msg);
    mqttClient.disconnect();
    Serial.println("Disconnected from broker");
    }
  else{
    Serial.println("Failed, state is:  ");
    Serial.println(mqttClient.state());
    }
}
  

void connect_to_wifi(){
  Serial.begin(115200);
  Serial.println("Connecting, please wait...");
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
    }
  Serial.println("");
  Serial.println("Connecting successful!");
  Serial.println("Received IP adress: ");
  Serial.println(WiFi.localIP());
}